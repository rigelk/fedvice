<?php

namespace App\Repository;

use App\Entity\Instance;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Instance|null find($id, $lockMode = null, $lockVersion = null)
 * @method Instance|null findOneBy(array $criteria, array $orderBy = null)
 * @method Instance[]    findAll()
 * @method Instance[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InstanceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Instance::class);
    }


    /**
     * Returns the number of instances in database
     * @return int|mixed
     */
    public function countEntries(){
        try {
            return $this->createQueryBuilder('i')
                ->select('count(i.id)')
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
            return 0;
        }
    }

    /**
     * Returns the number of social networks in database
     * @return mixed
     */
    public function countSocial(){
        return $this->createQueryBuilder('i')
            ->select('i.social')
            ->addSelect('count(i.id) as count')
            ->groupBy("i.social")
            ->orderBy("count","DESC")
            ->getQuery()
            ->getResult();
    }

    /**
     * Returns most used tags
     * @return mixed
     */
    public function mostUsedTags()
    {
        return $this->createQueryBuilder('i')
            ->innerJoin('i.tags', 'tags')
            ->select('tags.tag')
            ->addSelect('count(i.id) as count')
            ->groupBy("tags.tag")
            ->orderBy("count","DESC")
            ->setMaxResults(5)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * Find a host by uuid, it allows to know if a user already submitted the domain
     * @param $host
     * @param $uuid
     * @return mixed
     */
    public function findByHostUuid($host, $uuid)
    {
        return $this->createQueryBuilder('i')
            ->join('i.accounts', 'accounts')
            ->where('i.host = :host')
            ->setParameter('host', $host)
            ->andWhere('accounts.uuid = :uuid')
            ->setParameter('uuid', $uuid)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()
            ;
    }


    /**
     * Returns instances matching tags
     * @param $tag
     * @return mixed
     */
    public function findhostWithTag($tag)
    {
        return $this->createQueryBuilder('i')
            ->innerJoin('i.tags', 'tags')
            ->where('tags.tag = :tag')
            ->setParameter('tag', $tag)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * Returns tags matching an instance
     * @param $host
     * @return mixed
     */
    public function findTagsForHost($host)
    {
        return $this->createQueryBuilder('i')
            ->innerJoin('i.tags', 'tags')
            ->select("tags.tag")
            ->andWhere('i.host = :host')
            ->setParameter('host', trim($host))
            ->getQuery()
            ->getResult()
            ;
    }
}
