<?php
/**
 * Created by PhpStorm.
 * User: tom79
 * Date: 24/02/19
 * Time: 14:16
 */

namespace App\Command;


use App\Entity\Account;
use App\Entity\Instance;
use App\Entity\NotificationsLogs;
use App\Entity\Tag;
use Curl\Curl;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class ReadNotificationsCommand extends Command
{

    protected static $defaultName = 'read:notifications';
    private $container;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {
        $this->setDescription('Read the notification timeline of the account set in env')
        ->setHelp('Make sure you created a bot account from the web ui and that you created an application from this account.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $app_token = getenv("app_token");
        $app_host = getenv("app_host");
        $paperquotes_enabled = getenv("paperquotes_enabled");
        $paperquotes_key = getenv("paperquotes_key");

        $doctrine = $this->container->get('doctrine');
        //Retrieve the last read notification id
        $notificationLogs = $doctrine->getRepository("App:NotificationsLogs")->findOneBy([],["date" => "DESC"]);
        try {
            $curl = new Curl();
            $curl->setHeader('Authorization', 'Bearer '. $app_token);
            //Exclude favourites, follows and reblogs (the social network must accept these parameters)
            //Otherwise, you have to apply the filter in the API reply
            $params = "exclude_types[]=favourite&exclude_types[]=follow&exclude_types[]=reblog";
            //There was a previous call, so the min_id is set to this value
            if( $notificationLogs)
                $params .= "&min_id=" . $notificationLogs->getLastReadId();
            //The get call to the API with parameters
            $url = "https://" . $app_host . "/api/v1/notifications?" . $params;
            $notifications = $curl->get($url);
            //Initialize the min_id value
            $min_id = null;
            $response = $notifications->response;
            //The json reply is turned into an array
            $responseArray = \json_decode($response, true);
            //Loop through header reply to find the min_id value that will be recorded for next calls
            if( $notifications->response_headers && count($responseArray) > 0){
                foreach ($notifications->response_headers as $value){

                    if( strpos($value, 'Link: ') !== false){
                        preg_match(
                            "/min_id=([0-9a-zA-Z]{1,})/",
                            $value,
                            $matches
                        );
                        if( $matches){
                            $min_id = $matches[1];
                        }
                    }
                }
                //Initialize a NotificationsLogs entity to persist it
                $notificationLogs = new NotificationsLogs();
                $notificationLogs->setDate(new \DateTime());
                $notificationLogs->setLastReadId($min_id);
                $notificationLogs->setNewNotifications(count($responseArray));
                $doctrine->getManager()->persist($notificationLogs);
                $doctrine->getManager()->flush();
                //Loop through unread notifications
                foreach ($responseArray as $singleNotification){
                    $account = $singleNotification['account'];
                    $status = $singleNotification['status'];
                    $content = $status['content'];
                    $error_message = null;
                    $message = null;
                    $tags = $status['tags'];

                    //Detects if there is a URL inside the message
                    preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', strip_tags(str_replace("<" ," <",$content)), $match);
                    $urltoadd = null;
                    foreach ($match[0] as $match) {
                            $urltoadd = $match;
                    }
                    //No URL was found, so we check for a domain name
                    if( !$urltoadd){
                        preg_match_all('#(?!\-)(?:[a-zA-Z\d\-]{0,62}[a-zA-Z\d]\.){1,126}(?!\d+)[a-zA-Z\d]{1,63}#', strip_tags(str_replace("<" ," <",$content)), $match);
                        foreach ($match[0] as $match) {
                            $urltoadd = "https://".$match;
                        }
                    }
                    //=> $urltoadd contains the network URL or is null;
                    $social = null;
                    if( $urltoadd){
                        $parsedURL = parse_url(trim($urltoadd));
                        $host = trim($parsedURL['host'] ? $parsedURL['host'] : array_shift(explode('/', $parsedURL['path'], 2)));
                        $social = ucfirst(strtolower($this->getInstanceNodeInfo($host)));
                    }

                    //An account asks for removing its data in db
                    if( count($tags) == 0 && (strpos(strtolower($content), 'remove') !== false ||  strpos(strtolower($content), 'delete') !== false)){
                        $act_db = $doctrine->getRepository("App:Account")->findOneBy(["uuid"=>trim($account['url'])]);
                        if(! $act_db){
                            $message = "Hello  @".$account['acct'] . "! I didn't find data from your account to delete :)";
                        }else{
                            $doctrine->getManager()->remove($act_db);
                            $doctrine->getManager()->flush();
                            $message =  "Hello  @".$account['acct'] . "! All your data have been removed :)";
                        }
                        //An account ask for help
                    }else  if( count($tags) == 0 && (strpos(strtolower($content), 'help') !== false )){
                        $message =  "Hello  @".$account['acct'] . ", here is how I work: \r\n\r\n";
                        $message .= "For adding an instance:\r\n- Mention me with a domain (or a URL) with some #tags\r\n\r\n";
                        $message .= "For suggestions:\r\n- Mention me with #tags only\r\n\r\n";
                        $message .= "For removing your data:\r\n- Mention me with \"delete\" or \"remove\"";
                        //An account ask for stats
                    }else  if( count($tags) == 0 && (strpos(strtolower($content), 'stats') !== false )){
                        $numberInstances = $doctrine->getRepository("App:Instance")->countEntries();
                        $numberTags = $doctrine->getRepository("App:Tag")->countEntries();
                        $message =  "Hello  @".$account['acct'] . ", here are somes stats: \r\n\r\n";
                        $message .= "I know ".$numberInstances." instances\r\n";
                        $message .= "I know ".$numberTags." tags\r\n\r\n";
                        $socials = $doctrine->getRepository("App:Instance")->countSocial();
                        foreach ($socials as $social){
                            $message .= ucfirst(strtolower($social['social'])).": ".$social['count']."\r\n";
                        }
                        $tags = $doctrine->getRepository("App:Instance")->mostUsedTags();
                        $message .= "Most used tags: ";
                        foreach ($tags as $tag){
                            $message .= "#".$tag['tag']. " ";
                        }
                    }else{
                        if( $urltoadd){ //A URL was found in the mention
                            if( count($tags) > 0){ //There was tags
                                if( $social == null){ //The instance is not a valid one.
                                    $error_message = "Hello  @".$account['acct'] . "! I am sorry, but I didn't manage to know the network behind ". $host. " :(";
                                }else{
                                    //Check if the account already submitted a URL
                                    $act_db = $doctrine->getRepository("App:Account")->findOneBy(["uuid"=>trim($account['url'])]);
                                    //Account doesn't exist, it will be added
                                    $edit = false;
                                    $editdate = false;
                                    if( !$act_db){ //Insert account in db
                                        $act_db = new Account();
                                        $act_db->setUuid(trim($account['url']));
                                        $doctrine->getManager()->persist($act_db);
                                        $doctrine->getManager()->flush();
                                        $instance = $doctrine->getRepository("App:Instance")->findOneBy(["host"=>$host]);
                                        if( !$instance) //The submitted instance is unknown in the db
                                            $instance = new Instance();
                                        else //The instance is known, the date will be updated
                                            $editdate = true;
                                    }else{ //The account is already known
                                        $instanceArray[] = null;
                                        $instanceArray = $doctrine->getRepository("App:Instance")->findByHostUuid($host, $act_db->getUuid());
                                        if( $instanceArray && count($instanceArray) > 0) { //The account already submitted for this domain
                                            $instance = $instanceArray[0];
                                            $edit = true;
                                        }else { //It's a new submit for this account
                                            $instance = $doctrine->getRepository("App:Instance")->findOneBy(["host" => $host]);
                                            if( !$instance) // The instance is not known
                                                $instance = new Instance();
                                        }
                                    }
                                    $instance->addAccount($act_db); //Add account to the submitted instance
                                    foreach ($tags as $tag){ //Add tags
                                        $tag_db = $doctrine->getRepository("App:Tag")->findOneBy(["tag"=>trim($tag["name"])]);
                                        //Tag doesn't exist, it will be added
                                        if( !$tag_db){
                                            $tag_db = new Tag();
                                            $tag_db->setTag(trim($tag["name"]));
                                            $doctrine->getManager()->persist($tag_db);
                                            $doctrine->getManager()->flush();
                                        }
                                        $instance->addTag($tag_db);
                                    }
                                    $instance->setHost($host);
                                    if( $edit || $editdate) //Date is updated
                                        $instance->setUpdatedAt(new \DateTime());
                                    else //Creation date is set
                                        $instance->setCreatedAt(new \DateTime());
                                    $instance->setSocial(strtoupper($social));
                                    $doctrine->getManager()->persist($instance);
                                    $doctrine->getManager()->flush();
                                    if( !$edit) //Custom message
                                        $message = $paramsPost['status'] = "Hello  @".$account['acct'] . "! I added your suggestion for ".ucfirst($social) ." :)";
                                    else
                                        $message = "Hello  @".$account['acct'] . "! Your suggestion has been updated for ".ucfirst($social) ." :)";
                                }
                            }else{ //Here no tags were submitted and there is a URL
                                if( $social != null){ //This about a valid social network URL
                                    $tag_host = $doctrine->getRepository("App:Instance")->findTagsForHost($host);
                                    if( $tag_host){ //Related tags to this domain are sent if it is known
                                        $message = "Hello  @".$account['acct'] . "! Here are tags for ".$host ." :\r\n\r\n";
                                        foreach ($tag_host as $tag){
                                            $message .= "#".$tag['tag'] ." ";
                                        }
                                    }else{ // The instance is valid but there is no tags about it
                                        $error_message = "Hello  @".$account['acct'] . "! I am sorry, but this ". $social ." instance has not been added yet!";
                                    }
                                }else{ //This not a valid domain name
                                    $error_message = "Hello  @".$account['acct'] . "! I am sorry, but I didn't manage to know the network behind ". $host. " :(";
                                }
                            }
                        }else{ //There was no URL attached to domain
                            $tags = $status['tags'];
                            $host = array();
                            if( count($tags) > 0) { //The user submitted tags
                                foreach ($tags as $tag) {
                                    $instances = $doctrine->getRepository("App:Instance")->findhostWithTag($tag['name']);
                                    foreach ($instances as $ins){
                                        if( !isset($host[$ins->getHost()]))
                                            $host[$ins->getHost()] = 1;
                                        else
                                            $host[$ins->getHost()] = +1;
                                    }
                                }
                                if( count($host) > 0){
                                    $message = "Hello  @".$account['acct'] . "! Here is my suggestion:\r\n";
                                    $total = 0 ;
                                    foreach ($host as $key => $value){
                                        $total += $value;
                                    }
                                    $count = 0;
                                    foreach ($host as $key => $value){
                                        $message .= "https://".$key. " - ".round($value/$total * 100)."%\r\n";
                                        $count++;
                                        if( $count > 5 )
                                            break;
                                    }
                                }else{ //There was no instance found
                                    $error_message = "Hello  @".$account['acct'] . "! I didn't find an instance matching your tags :(";
                                }
                            }else{ //No tags, no URL... Let's go for the fun if the API is enabled
                                if($paperquotes_enabled){
                                    $rand = rand ( 0 , 40000 );
                                    $curl = new Curl();
                                    $curl->setHeader('Authorization', 'Token '. $paperquotes_key);
                                    $curl->setHeader('Content-Type', 'application/json');
                                    $url = "http://api.paperquotes.com/apiv1/quotes/?limit=1&tag=famous&lang=en&maxlength=300&offset=".$rand;
                                    $quote = $curl->get($url);
                                    $response = $quote->response;
                                    //The json reply is turned into an array
                                    $responseArray = \json_decode($response, true);
                                    $author = $responseArray['results'][0]['author'];
                                    $quote = $responseArray['results'][0]['quote'];
                                    if( !empty($author))
                                        $message = "Hello  @".$account['acct'] . " :) I have a quote from ". $author . ":\r\n\r\n";
                                    else
                                        $message = "Hello  @".$account['acct'] . " :) I have a quote \r\n\r\n";
                                    $message .= $quote;
                                }

                            }
                        }
                    }
                    $curl = new Curl();
                    $curl->setHeader('Authorization', 'Bearer '. $app_token);
                    $paramsPost['in_reply_to_id'] = $status['id'];
                    $paramsPost['visibility'] = $status['visibility'];
                    if( $error_message)
                        $paramsPost['status'] = $error_message;
                    else
                        $paramsPost['status'] = $message;
                    $url = "https://" . $app_host . "/api/v1/statuses";
                    $curl->post($url, $paramsPost);
                }
            }
        } catch (\ErrorException $e) {
        } catch (\Exception $e) {
        }
    }


    private function getInstanceNodeInfo($host){
        try {
            $curl = new Curl();
            $url = "https://" . $host . "/.well-known/nodeinfo";
            $reply = $curl->get($url);


            $responseArray = \json_decode($reply->response, true);
            if ( empty($responseArray)){
                $curl = new Curl();
                $url = "https://" . $host . "/api/v1/instance";
                $reply = $curl->get($url);
                $responseArray = \json_decode($reply->response, true);
                if ( empty($responseArray)){
                    return null;
                }else{
                    return "MASTODON";
                }
            }else{
                $url = $responseArray["links"][count($responseArray["links"])-1]["href"];
                $curl = new Curl();
                $reply = $curl->get($url);
                $responseArray = \json_decode($reply->response, true);
                return strtoupper($responseArray["software"]["name"]);
            }

        } catch (\ErrorException $e) {

        }
    }
}